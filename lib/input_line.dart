// Packages
import 'package:flutter/material.dart';

class InputLine extends StatelessWidget {
  final Widget input;
  final IconData icon;
  final VoidCallback onPressed;

  InputLine(this.input, this.icon, this.onPressed);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 30.0),
        ),
        Expanded(
          child: input,
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 30.0),
          child: FloatingActionButton(
            onPressed: onPressed,
            child: Icon(icon),
          ),
        ),
      ],
    );
  }
}
