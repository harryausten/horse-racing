// Packages
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
// UI
import './input_line.dart';
import './player_name_list.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Horse Racing',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Horse Racing'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({this.title});
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final playerNameTextField = TextEditingController();
  final horseNumTextField = TextEditingController();
  List<String> players = [];
  int numHorses = 0;
  PlayerNameList list;

  void _addPlayer() {
    setState(() {
      players.add(playerNameTextField.text);
    });
    playerNameTextField.clear();
  }

  void _update() {
    setState(() {});
  }

  List<Widget> createNumberList(int n) {
    List<Widget> ret = List(n);
    for (int i = 0; i < n;) {
      ret[i] = Container(
        alignment: Alignment.center,
        child: Text(
          (++i).toString(),
          style: TextStyle(
            fontSize: 30.0,
          ),
        ),
      );
    }
    return ret;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 10.0),
            ),
            InputLine(
              TextField(
                keyboardType: TextInputType.text,
                controller: playerNameTextField,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Enter player name',
                ),
              ),
              Icons.add,
              _addPlayer,
            ),
            PlayerNameList(
              players: players,
              numHorses: numHorses,
            ),
            InputLine(
              Container(
                height: 100.0,
                child: CupertinoPicker(
                  backgroundColor: null,
                  itemExtent: 40.0,
                  onSelectedItemChanged: (int n) => numHorses = ++n,
                  children: createNumberList(30),
                  looping: true,
                  useMagnifier: true,
                ),
              ),
              Icons.refresh,
              _update,
            ),
            Padding(
              padding: EdgeInsets.only(top: 10.0),
            ),
          ],
        ),
      ),
    );
  }
}
