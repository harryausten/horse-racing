// Packages
import 'package:flutter/material.dart';
import 'dart:collection';

class PlayerNameList extends StatelessWidget {
  final List<String> players;
  final int numHorses;

  PlayerNameList({@required this.players, this.numHorses = 0});

  Widget createPlayer(String name, [int n]) {
    return Dismissible(
    key: Key(name),
    background: Container(
      color: Colors.red,
    ),
    child: ListTile(
      leading: n != null
          ? CircleAvatar(
              child: Text(n.toString()),
            )
          : null,
      title: Text(
        name,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 24.0),
      ),
    ),
    onDismissed: (direction) {
      players.remove(name);
    },
  );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> list = new List(players.length);

    if (numHorses > 0) {
      HashMap<String, int> map = HashMap();

      List<int> horses = [];
      horses.length = numHorses;
      while (players.length > horses.length) {
        horses.length *= 2;
      }

      for (int i = 0; i < horses.length; i++) {
        horses[i] = i % numHorses + 1;
      }
      horses.shuffle();
      for (int i = 0; i < players.length; i++) {
        map[players[i]] = horses[i];
      }

      for (int i = 0; i < players.length; i++) {
        list[i] = createPlayer(players[i], map[players[i]]);
      }
    } else {
      for (int i=0; i<players.length; i++) {
        list[i] = createPlayer(players[i]);
      }
    }

    return Expanded(
      child: ListView(
        children: list,
      ),
    );
  }
}
